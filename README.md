## Car Management API : JWT and bcrypt
```
`Team 4`
- Fajar Satrio Utomo
- Aufar Rifqi Athallah
- Nur Zidan Haq
- Indira Virliana
- Dandy Gilang Saputra

```

### Run server
```
npm start
```

### Database Management
- `npm run db:create` : to create a database
- `npm run db:migrate` : to run database migration
- `npm run db:seed` : to run seed. create a superadmin account

### Open API Documentations : Swagger UI
```
/api/api-docs
```

### Open API Documentations : Open Api Json
```
/api/api-json
```

### Superadmin Account
Only superadmin can create admin account
```
email: admin@email.com
password: admin
```
